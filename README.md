# Unused Geometric Patterns

This repository is a collection of geometric patterns originally
created for earlier, now-retired versions of the [Pattern Parts]
website.

The `patterns` directory contains geometric patterns in
corresponding PDF and SVG formats. A few PDF format patterns 
have no SVG counterparts.

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Creative Commons License" 
       style="border-width:0" 
       src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png"/>
</a>

These works are licensed under a
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  Creative Commons Attribution-ShareAlike 4.0 International License
</a>.


[Pattern Parts]: http://parts.ornamentist.org
